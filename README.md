**Backgammon** 
(Android phone game)



Description:


    *   Two player game

    *   Backgammon classic rules https://en.wikipedia.org/wiki/Backgammon

    *   Type of players: human, bot

    *   Score tracking and statistics

    *   Easy to pause and continue game after
